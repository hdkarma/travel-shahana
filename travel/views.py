import pdb

from django.shortcuts import render
from rest_framework.generics import ListAPIView
from rest_framework.permissions import AllowAny

from essential.models import Customer, Stay, Booking
from essential.serializers import CustomerSerializer, StaySerializer, BookingSerializer


def front_page_view(request):
	form = request.GET.get("submit-form")
	if form:
		rating = request.GET.get("rating")
		travel_distance = request.GET.get("travel_distance")

		bookings = Booking.objects.filter(rating__gte=float(rating)).filter(stay__dest_type=request.GET.get("dest_type")).filter(stay__loc__lte=float(travel_distance))
		return render(request, 'list.html', {'bookings': bookings})
	return render(request, 'index.html')


class CustomerListAPIView(ListAPIView):
	permission_classes = [AllowAny]
	queryset = Customer.objects.all()
	serializer_class = CustomerSerializer


class StayListAPIView(ListAPIView):
	permission_classes = [AllowAny]
	queryset = Stay.objects.all()
	serializer_class = StaySerializer


class BookingListAPIView(ListAPIView):
	permission_classes = [AllowAny]
	queryset = Booking.objects.all()
	serializer_class = BookingSerializer
