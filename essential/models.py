from django.db import models


STAY_CHOICES = (
    ('resort', 'Resort'),
    ('home_stay', 'Home stay'),
    ('college', 'College'),
    ('hostel', 'Hostel'),
)
DEST_CHOICES = (
    ('hill_side', 'Hill side'),
    ('back_water', 'Back water'),
    ('beach', 'Beach'),
)


class Stay(models.Model):
    stay_name = models.CharField(max_length=50, default="", blank=True)
    stay_type = models.CharField(max_length=50, default="", blank=True, choices=STAY_CHOICES)
    dest_type = models.CharField(max_length=50, default=1, blank=True, choices=DEST_CHOICES)
    loc = models.FloatField(blank=False)

    def __str__(self):
        return self.stay_name


class Customer(models.Model):
    customer_name = models.CharField(max_length=50, default="", blank=True)

    def __str__(self):
        return self.customer_name


class Booking(models.Model):
    customer = models.ForeignKey(Customer, default=None, blank=True, null=True, related_name="customer_id", on_delete=models.CASCADE)
    stay = models.ForeignKey(Stay, default=None, blank=True, null=True, related_name="stay_id", on_delete=models.CASCADE)
    rating = models.FloatField(default=0.0, blank=True)

    def __str__(self):
        return self.customer.customer_name + " = " + self.stay.stay_name

