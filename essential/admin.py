from django.contrib import admin

# Register your models here.
from essential.models import Booking, Stay, Customer

admin.site.register(Stay)
admin.site.register(Booking)
admin.site.register(Customer)