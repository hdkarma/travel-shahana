from rest_framework.serializers import ModelSerializer

from essential.models import Customer, Stay, Booking


class CustomerSerializer(ModelSerializer):
    class Meta:
        model = Customer
        fields = "__all__"


class StaySerializer(ModelSerializer):
    class Meta:
        model = Stay
        fields = "__all__"


class BookingSerializer(ModelSerializer):
    customer = CustomerSerializer(many=True, read_only=True)
    stay = StaySerializer(many=True, read_only=True)

    class Meta:
        model = Booking
        fields = "__all__"
